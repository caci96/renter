import requests
import json
import glob


LISTS_PATH = 'raw_data/lists/'
POSTS_PATH = 'raw_data/posts/'


def process_post(data_in):
    if 'widgets' not in data_in:
        return False
    data_out = [
        data_in['token'],
        data_in['widgets']['location'].get('latitude'),
        data_in['widgets']['location'].get('longitude'),
        data_in['data']['webengage']['credit'],
        data_in['data']['webengage']['rent'],
        data_in['widgets']['header']['title'],
        data_in['widgets']['images'],
    ]
    for i in data_out[:5]:
        if i is None:
            return False
    data_out[1] = float(data_out[1])
    data_out[2] = float(data_out[2])
    data_out[3] = data_out[3] / 1000000.0
    data_out[4] = data_out[4] / 1000000.0
    return data_out


def main():
    ls = glob.glob(POSTS_PATH + '*.json')
    data = []
    for filename in ls:
        with open(filename) as f:
            data.append(process_post(json.loads(f.read())))
    data = [d for d in data if d]
    with open('../front-end/static/data/summary.json', 'w') as f:
        f.write(json.dumps(data))
    print(len(data))


if __name__ == '__main__':
    main()
