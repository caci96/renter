import requests
import json
import glob


LISTS_PATH = 'raw_data/lists/'
POSTS_PATH = 'raw_data/posts/'
RETRIES = 5


def extract_list_of_posts():
    ls = glob.glob(LISTS_PATH + '*.json')
    data = []
    for filename in ls:
        with open(filename) as f:
            data += [post['data']['token']
                     for post in json.loads(f.read())['widget_list']]
    return set(data)


def extract_available_posts():
    ls = glob.glob(POSTS_PATH + '*.json')
    posts = [post.split('/')[-1].split('.json')[0]for post in ls]
    return set(posts)


def download_post(post_id):
    url = 'https://api.divar.ir/v5/posts/%s/' % post_id
    r = requests.get(url)
    assert r.status_code in [200, 301], (r.status_code, post_id)
    data = r.json()
    with open(POSTS_PATH + '/%s.json' % post_id, 'w') as f:
        f.write(json.dumps(data, indent=4, sort_keys=True))
    if r.status_code == 200:
        print(post_id, data['widgets']['header']['title'])


def main():
    to_crawl = extract_list_of_posts()
    crawled = extract_available_posts()
    remaining = to_crawl - crawled
    print('Total:', len(to_crawl))
    print('Done:', len(crawled))
    print('Remaining:', len(remaining))
    for post in remaining:
        download_post(post)


if __name__ == '__main__':
    main()
